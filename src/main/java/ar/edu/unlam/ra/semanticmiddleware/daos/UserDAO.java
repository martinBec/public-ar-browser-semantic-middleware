package ar.edu.unlam.ra.semanticmiddleware.daos;

import ar.edu.unlam.ra.semanticmiddleware.model.Authority;
import ar.edu.unlam.ra.semanticmiddleware.model.AuthorityTypes;
import ar.edu.unlam.ra.semanticmiddleware.model.User;
import ar.edu.unlam.ra.semanticmiddleware.repositories.UserRepository;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository("userDAO")
public class UserDAO extends BaseDAO<User> implements UserRepository {

    @Override
    public List<User> findAll(Pageable page) {
        CriteriaBuilder builder = createCriteriaBuilder();

        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);

        Join<User, Authority> joinAuthority = (Join) root.fetch("authorities");
        Predicate roleTypePredicate = builder.equal(joinAuthority.get("authority"), AuthorityTypes.USER);

        criteria = criteria.select(root).where(roleTypePredicate);

        Query<User> query = this.createQuery(criteria);
        query = pagedQuery(query,page);

        return query.getResultList();
    }

    public List<User> findUsersByUsername(String username, Pageable page) {
        // used by APIs

        CriteriaBuilder builder = createCriteriaBuilder();

        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);

        Join<User, Authority> joinAuthority = (Join) root.fetch("authorities");
        Predicate usernamePredicate = builder.like(root.get("username"), "%" + username + "%");
        Predicate roleTypePredicate = builder.equal(joinAuthority.get("authority"), AuthorityTypes.USER);
        List<Predicate> whereConditions = new ArrayList<>();
        whereConditions.add(usernamePredicate);
        whereConditions.add(roleTypePredicate);

        criteria = criteria.select(root).where(whereConditions.toArray(new Predicate[]{}));

        Query<User> query = this.createQuery(criteria);
        query = pagedQuery(query,page);

        return query.getResultList();
    }

    public Optional<User> findUserByUsername(String username){
        //Used by Authenticator
        Session session = entityManager.unwrap(Session.class);
        CriteriaBuilder builder = session.getCriteriaBuilder();

        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);

        Join<User, Authority> joinAuthority = (Join) root.fetch("authorities");
        Predicate roleTypePredicate = builder.equal(joinAuthority.get("authority"), AuthorityTypes.USER);
        Predicate usernamePredicate = builder.like(root.get("username"), username);
        List<Predicate> whereConditions = new ArrayList<>();
        whereConditions.add(usernamePredicate);
        whereConditions.add(roleTypePredicate);

        criteria = criteria.select(root).where(whereConditions.toArray(new Predicate[]{}));

        Query<User> query = session.createQuery(criteria);

        return query.uniqueResultOptional();

    }

    public Optional<User> findById(long id) {
        Session session = entityManager.unwrap(Session.class);
        return Optional.of(session.get(User.class, id));
    }

    @Override
    public Boolean exists(Example<User> user) {
        return this.findUserByUsername(user.getProbe().getUsername()).isPresent();
    }

}