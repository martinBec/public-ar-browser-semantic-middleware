package ar.edu.unlam.ra.semanticmiddleware.daos;

import ar.edu.unlam.ra.semanticmiddleware.services.Constants;
import java.util.ArrayList;
import java.util.List;

public final class ProcedureDao {

    public static String searchProcedures(String titleSubString, int page) {

        final int offset = (Constants.PAGE_SIZE * (page - 1));

        return "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "\n" +
                "CONSTRUCT\n" +
                "{\n" +
                "    ?procedure ars:hasProcedureTitle ?title;\n" +
                "               rdf:type ars:Procedure.\n" +
                "}\n" +
                "WHERE\n" +
                "{\n" +
                "    ?procedure ars:hasProcedureTitle ?title.\n" +
                "    FILTER regex(?title,\"/*" + titleSubString +"/*\")\n" +
                "}\n" +
                "ORDER BY ?title \n" +
                "LIMIT " + Constants.PAGE_SIZE + "\n" +
                "OFFSET " + offset;
    }

    public static String searchProceduresByAuthor(String authorLocalId, String titleSubString, int page) {

        final int offset = (Constants.PAGE_SIZE * (page - 1));

        String searchProceduresByAuthorQuery;

        if (titleSubString.isEmpty()) {
            searchProceduresByAuthorQuery = "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                    "\n" +
                    "CONSTRUCT\n" +
                    "{\n" +
                    "    ?procedure ars:hasProcedureTitle ?title;\n" +
                    "               rdf:type ars:Procedure;\n" +
                    "}\n" +
                    "WHERE\n" +
                    "{\n" +
                    "    ?procedure ars:hasProcedureTitle ?title;\n" +
                    "               ars:hasAuthor ars:" + authorLocalId +";\n" +
                    "}\n" +
                    "ORDER BY ?title \n" +
                    "LIMIT " + Constants.PAGE_SIZE + "\n" +
                    "OFFSET " + offset;
        }
        else {
            searchProceduresByAuthorQuery = "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                    "\n" +
                    "CONSTRUCT\n" +
                    "{\n" +
                    "    ?procedure ars:hasProcedureTitle ?title;\n" +
                    "               rdf:type ars:Procedure;\n" +
                    "}\n" +
                    "WHERE\n" +
                    "{\n" +
                    "    ?procedure ars:hasProcedureTitle ?title;\n" +
                    "               ars:hasAuthor ars:" + authorLocalId +";\n" +
                    "    FILTER regex(?title,\"/*" + titleSubString +"/*\")\n" +
                    "}\n" +
                    "ORDER BY ?title \n" +
                    "LIMIT " + Constants.PAGE_SIZE + "\n" +
                    "OFFSET " + offset;
        }

        return searchProceduresByAuthorQuery;
    }

    public static String getProcedureDetail(String procedureId) {

        return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "\n" +
                "CONSTRUCT\n" +
                "{\n" +
                     procedureId + " ars:hasProcedureTitle ?title;\n" +
                "                           rdf:type ars:Procedure;\n" +
                "                           ars:hasProcedureDescription ?description;\n" +
                "                           ars:hasAuthor ?author;\n" +
                "                           ars:firstStep ?firstStep.\n" +
                "    ?author    ars:hasUsername ?username;\n" +
                "               rdf:type ars:Author.\n" +
                "    ?firstStep ars:hasStepTitle ?stepTitle;\n" +
                "               ars:hasStepDescription ?stepDescription;\n" +
                "               ars:nextStep ?nextStep;\n" +
                "               rdf:type ars:Step.\n" +
                "}\n" +
                "WHERE\n" +
                "{\n" +
                      procedureId + " ars:hasProcedureTitle ?title;\n" +
                "                           ars:hasProcedureDescription ?description;\n" +
                "                           ars:hasAuthor ?author.\n" +
                "     ?author ars:hasUsername ?username.\n" +
                "    \n" +
                "    OPTIONAL {\n" +
                         procedureId + " ars:firstStep ?firstStep.\n" +
                "        ?firstStep ars:hasStepTitle ?stepTitle;\n" +
                "                   ars:hasStepDescription ?stepDescription.\n" +
                "    }\n" +
                "    OPTIONAL {\n" +
                        procedureId + " ars:firstStep ?firstStep.\n" +
                "       ?nextStep ars:previousStep ?firstStep.\n" +
                "    }\n" +
                "}";
    }

    @Deprecated
    public static String insertProcedures(String procedureLocalId, String procedureTitle,String procedureDescription, String authorLocalId) {

        return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "\n" +
                "INSERT DATA\n" +
                "{\n" +
                "  ars:"+ procedureLocalId +" rdf:type ars:Procedure;\n" +
                "            ars:hasProcedureTitle \"" + procedureTitle +"\";\n" +
                "            ars:hasProcedureDescription \"" + procedureDescription + "\";\n" +
                "            ars:hasAuthor ars:" + authorLocalId + ".\n" +
                "}";
    }

    //////////////////////////////////////////////// Procedure Editor ////////////////////////////////////////////////

    public static String procedureTuples(String procedureLocalId, String procedureTitle, String procedureDescription, String authorLocalId) {

        return  "  ars:"+ procedureLocalId +" rdf:type ars:Procedure;\n" +
                "  ars:hasProcedureTitle \"" + procedureTitle +"\";\n" +
                "  ars:hasProcedureDescription \"" + procedureDescription + "\";\n" +
                "  ars:hasAuthor ars:" + authorLocalId + ".\n";
    }

    public static String insertProcedureQuery(String procedureTuples, String firstStepTuples, String nextStepsTuples) {
        return  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "\n" +
                "INSERT DATA \n" +
                "{\n" +
                   procedureTuples + "\n" +
                   firstStepTuples + "\n" +
                   nextStepsTuples + "\n" +
                "}";
    }

    public static String procedureOfAuthorEditorResponse(String procedureLocalId) { //used in Procedure insertion from editor response

        return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "\n" +
                "CONSTRUCT\n" +
                "{\n" +
                "  ars:" + procedureLocalId + " rdf:type ars:Procedure;\n" +
                "             ars:hasProcedureTitle ?title;\n" +
                "             ars:hasProcedureDescription ?description;\n" +
                "             ars:hasAuthor ?author;\n" +
                "             ars:firstStep ?firstStep.\n" +
                "  ?author    rdf:type ars:Author;\n" +
                "             ars:hasUsername ?username.\n" +
                "  ?firstStep ars:hasStepTitle ?stepTitle;\n" +
                "             ars:hasStepDescription ?stepDescription;\n" +
                "             ars:nextStep ?nextStep;\n" +
                "             rdf:type ars:Step.\n" +
                "}\n" +
                "WHERE\n" +
                "{\n" +
                "   ars:" + procedureLocalId + " ars:hasProcedureTitle ?title;\n" +
                "                     ars:hasProcedureDescription ?description;\n" +
                "                     ars:hasAuthor ?author.\n" +
                "   ?author           ars:hasUsername ?username.\n" +
                "    OPTIONAL {\n" +
                "        ars:" + procedureLocalId + " ars:firstStep ?firstStep.\n" +
                "        ?firstStep ars:hasStepTitle ?stepTitle;\n" +
                "                   ars:hasStepDescription ?stepDescription.\n" +
                "    }\n" +
                "    OPTIONAL {\n" +
                "       ars:" + procedureLocalId + " ars:firstStep ?firstStep.\n" +
                "       ?nextStep ars:previousStep ?firstStep.\n" +
                "    }\n" +
                "}";
    }

    public static String editProcedureDataQuery(String procedureId, String procedureTitle, String procedureDescription, String firstStep) {

        List<String> tuplesToInsert = new ArrayList<>();
        List<String> tuplesToDelete = new ArrayList<>();

        if (procedureTitle != null) {
            tuplesToInsert.add(procedureId + " ars:hasProcedureTitle \"" + procedureTitle +"\".\n");
            tuplesToDelete.add(procedureId + " ars:hasProcedureTitle ?title.\n");
        }

        if (procedureDescription != null) {
            tuplesToInsert.add(procedureId + " ars:hasProcedureDescription \"" + procedureDescription +"\".\n");
            tuplesToDelete.add(procedureId + " ars:hasProcedureDescription ?description.\n");
        }

        if (firstStep != null) {
            tuplesToInsert.add(procedureId + " ars:firstStep " + firstStep +".\n");
            tuplesToDelete.add(procedureId + " ars:firstStep ?firstStep.\n");
        }

        if (tuplesToInsert.isEmpty()) {
            return "";
        }

        String insertStatements = tuplesToStatementsString(tuplesToInsert);
        String deleteStatements = tuplesToStatementsString(tuplesToDelete);

        return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "\n" +
                "DELETE\n" +
                "{\n" +
                    deleteStatements +
                "}\n" +
                "INSERT\n" +
                "{\n" +
                    insertStatements +
                "}\n" +
                "WHERE\n" +
                "{\n" +
                   deleteStatements +
                "}";
    }

    private static String tuplesToStatementsString(List<String> tuples) {
        return tuples.stream().reduce("", (partialString, element) -> partialString + element);
    }
}
