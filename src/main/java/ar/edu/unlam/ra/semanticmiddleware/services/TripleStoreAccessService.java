package ar.edu.unlam.ra.semanticmiddleware.services;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.JSONLDMode;
import org.eclipse.rdf4j.rio.helpers.JSONLDSettings;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@Service
public final class TripleStoreAccessService {

    private final Repository repository;

    public TripleStoreAccessService() {
        //        SPARQLRepository sparqlRepository =  new SPARQLRepository(
//                "http://192.168.0.6:7200/repositories/procedure-test",
//                "http://192.168.0.6:7200/repositories/procedure-test/statements"
//        );

        SPARQLRepository sparqlRepository =  new SPARQLRepository(
                "http://localhost:82/sparql"
        );

        sparqlRepository.setUsernameAndPassword("admin","Passw0rd1");
        this.repository = sparqlRepository;
    }

    String performMassUpdate(String[] queries, String response) {

        if (queries.length > 0) {
            this.performMassUpdate(queries);
        }

        return this.perform(response);
    }

    void performMassUpdate(String[] queries) {
        RepositoryConnection conn = this.repository.getConnection();

        ThreadPoolExecutor executor =
                (ThreadPoolExecutor) Executors.newCachedThreadPool();
        ExecutorService executorService =
                MoreExecutors.getExitingExecutorService(executor,
                        100, TimeUnit.MILLISECONDS);

        ListeningExecutorService listeningExecutorService =
                MoreExecutors.listeningDecorator(executorService);


        List<ListenableFuture<?>> futures = new ArrayList<>();

        for (String query : queries) {
            ListenableFuture<?> future = listeningExecutorService.submit(() -> {
                conn.prepareUpdate(QueryLanguage.SPARQL,query).execute();
            });
            futures.add(future);
        }

        try {
            List<Object> objects = new ArrayList<>(Futures.allAsList(futures).get());
            conn.close();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            conn.close();
        }
    }

    String perform(String query) {
        try {
            RepositoryConnection conn = this.repository.getConnection();
            OutputStream output = this.performQuery(query,conn);
            conn.close();
            return output.toString();
        } catch (QueryEvaluationException e) {
            return "{}";
        }
    }

    private OutputStream performQuery(String query, RepositoryConnection conn) throws QueryEvaluationException {
        OutputStream output = new ByteArrayOutputStream();
        RDFWriter writer = Rio.createWriter(RDFFormat.JSONLD, output);

        writer.getWriterConfig()
                .set(JSONLDSettings.JSONLD_MODE, JSONLDMode.COMPACT)
                .set(JSONLDSettings.COMPACT_ARRAYS, true)
                .set(JSONLDSettings.HIERARCHICAL_VIEW, false)
                .set(JSONLDSettings.OPTIMIZE, true)
                .set(JSONLDSettings.USE_RDF_TYPE, true);

        GraphQueryResult result = conn.prepareGraphQuery(QueryLanguage.SPARQL, query)
            .evaluate();

        writer.startRDF();
        writer.handleNamespace("ars","http://www.raunlam.edu.ar/ontology/arsemanticprocedures#");
        result.stream().forEach(writer::handleStatement);
        writer.endRDF();

        return output;
    }

    String performUpdate(String updateQuery, String retrieveResultQuery) {
        RepositoryConnection conn = this.repository.getConnection();
        conn.prepareUpdate(QueryLanguage.SPARQL,updateQuery).execute();
        conn.close();
        return this.perform(retrieveResultQuery);
    }

    public void performUpdate(String updateQuery) {
        RepositoryConnection conn = this.repository.getConnection();
        conn.prepareUpdate(QueryLanguage.SPARQL,updateQuery).execute();
        conn.close();
    }
}
