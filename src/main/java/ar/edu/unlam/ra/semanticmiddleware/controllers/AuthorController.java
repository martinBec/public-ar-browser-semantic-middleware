package ar.edu.unlam.ra.semanticmiddleware.controllers;

import ar.edu.unlam.ra.semanticmiddleware.model.User;
import ar.edu.unlam.ra.semanticmiddleware.services.ProceduresService;
import ar.edu.unlam.ra.semanticmiddleware.services.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("api/v1/author")
public class AuthorController {

    UserSession sessionService;
    ProceduresService procedureService;

    @Autowired
    public AuthorController(ProceduresService procedureService, UserSession sessionService) {
        this.procedureService = procedureService;
        this.sessionService = sessionService;
    }

    @GetMapping(path = "/procedures", produces = "application/json")
    public String search(@RequestParam(value = "procedure_title", required = false, defaultValue = "") String procedureTitleSubString,
                         @RequestParam(value = "page", required = false, defaultValue = "1") int page) {

        User user = sessionService.getUser();
        return procedureService.searchProceduresByAuthorContaining(user.getSemanticLocalId(), procedureTitleSubString, page);
    }
}