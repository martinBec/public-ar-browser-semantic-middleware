package ar.edu.unlam.ra.semanticmiddleware.requestBodies.edition;

import ar.edu.unlam.ra.semanticmiddleware.model.Step;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class EditProcedureStepDTO {

    private String id;
    private String title;
    private String description;
    private String previousStep;

    public EditProcedureStepDTO(String id, String title, String description, String previousStep) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.previousStep = previousStep;
    }

    public Step build() {
        return new Step(this.id, this.title, this.description, this.previousStep);
    }
}
