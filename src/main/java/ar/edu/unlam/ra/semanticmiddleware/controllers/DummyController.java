package ar.edu.unlam.ra.semanticmiddleware.controllers;

import ar.edu.unlam.ra.semanticmiddleware.model.User;
import ar.edu.unlam.ra.semanticmiddleware.services.MockDataService;
import ar.edu.unlam.ra.semanticmiddleware.services.ProceduresService;
import ar.edu.unlam.ra.semanticmiddleware.services.StepService;
import ar.edu.unlam.ra.semanticmiddleware.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("api/v1/debug")
public final class DummyController {

    private UserServiceImpl userService;
    private MockDataService mock;

    @Autowired
    public  DummyController (UserServiceImpl userService, MockDataService mock) {
        this.userService = userService;
        this.mock = mock;
    }

    @PostMapping("/load")
    void debug() {
        User testUser  = new User("martinTest","123456");
        testUser = userService.register(testUser);

        if (testUser == null) {
            return;
        }

        for (int i = 0; i < 21; i++) {
            this.mock.createProcedure(i,testUser.getSemanticLocalId(),4);
        }
    }

    @PostMapping("/load_small_example")
    void debugSmallExample() {
        for (int i = 0; i < 100; i++) {
            this.mock.createMockExample("user example " + (i + 1) ,10,10);
        }
    }

    @PostMapping("/load_medium_example")
    void debugMediumExample() {
        for (int i = 0; i < 500; i++) {
            this.mock.createMockExample("user example " + (i + 1) ,10,20);
        }
    }

    @PostMapping("/load_big_example")
    void debugBigExample() {
        for (int i = 0; i < 1000; i++) {
            this.mock.createMockExample("user example " + (i + 1) ,10,50);
        }
    }
}
