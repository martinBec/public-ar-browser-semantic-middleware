package ar.edu.unlam.ra.semanticmiddleware.security;

import ar.edu.unlam.ra.semanticmiddleware.model.User;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import java.util.Date;

import static ar.edu.unlam.ra.semanticmiddleware.security.SecurityConstants.EXPIRATION_TIME;
import static ar.edu.unlam.ra.semanticmiddleware.security.SecurityConstants.SECRET;

public class JWTUtils {

    public static String generateTokenFor(User user) {
        return JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SECRET.getBytes()));
    }
}
