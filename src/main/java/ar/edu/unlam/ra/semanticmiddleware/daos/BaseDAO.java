package ar.edu.unlam.ra.semanticmiddleware.daos;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository("baseDAO")
public class BaseDAO<T> {

    @PersistenceContext
    protected EntityManager entityManager;

    protected Session  unWrapSession() {
        return entityManager.unwrap(Session.class);
    }

    protected CriteriaBuilder createCriteriaBuilder() {
        return unWrapSession().getCriteriaBuilder();
    }

    protected Query<T> createQuery(CriteriaQuery<T> criteria) {
        return unWrapSession().createQuery(criteria);
    }

    protected Query<T> pagedQuery(Query<T> query, Pageable page){
        query.setFirstResult(page.getPageNumber() * page.getPageSize());
        query.setMaxResults(page.getPageSize());
        return query;
    }

    public List<T> findAll(Class<T> type,Pageable page) {
        CriteriaBuilder builder = createCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(type);
        Root<T> root = criteria.from(type);
        Query<T> query = this.createQuery(criteria);
        query = pagedQuery(query,page);
        return query.getResultList();
    }

    @Transactional
    public T save(T object) {
        Session session = entityManager.unwrap(Session.class);
        try {
            session.saveOrUpdate(object);
            return object;
        } catch (Exception e) {

            return null;
//            e.printStackTrace();
//            throw e;
        }
    }
    @Transactional
    public Boolean delete(T object) {
        Session session = entityManager.unwrap(Session.class);
        try {
            session.delete(object);
            return true;
        } catch (Exception e) {
            return  false;
//            e.printStackTrace();
//            throw new Exception(e.getMessage());
        }
    }
}