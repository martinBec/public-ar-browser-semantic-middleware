package ar.edu.unlam.ra.semanticmiddleware.model;

import lombok.Getter;

@Getter
public final class Step {
    final String id;
    final String title;
    final String description;
    final String previousStep;

    public Step(String id, String title, String description, String previousStep) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.previousStep = previousStep;
    }

    public Step(String title, String description) {
        this(null, title, description,null);
    }
}
