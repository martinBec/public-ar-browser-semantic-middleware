package ar.edu.unlam.ra.semanticmiddleware.controllers;

import ar.edu.unlam.ra.semanticmiddleware.services.StepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/procedures/steps")
public class ProcedureStepController {

    StepService stepsService;

    @Autowired
    public ProcedureStepController(StepService stepsService) {
        this.stepsService = stepsService;
    }

    @GetMapping(path = "/get", produces = "application/json")
    public String getProcedureSteps(@RequestParam(value = "procedure_id", required = true) String procedureLocalId) {
        return stepsService.getStepsForProcedureLocalId(procedureLocalId);
    }
}
