package ar.edu.unlam.ra.semanticmiddleware.requestBodies.Entities;

import ar.edu.unlam.ra.semanticmiddleware.model.Step;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StepDTO {
    private String title;
    private String description;

    public StepDTO(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Step build() {
        return new Step(title,description);
    }
}
