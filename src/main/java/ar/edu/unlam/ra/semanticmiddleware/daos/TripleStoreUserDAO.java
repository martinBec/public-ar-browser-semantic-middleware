package ar.edu.unlam.ra.semanticmiddleware.daos;

import ar.edu.unlam.ra.semanticmiddleware.model.User;

import java.util.UUID;

public final class TripleStoreUserDAO {

    public static String insertUser(User user) {

        String userLocalID = UUID.randomUUID().toString();
        user.setSemanticLocalId(userLocalID);

        return insertUserInTripleStore(userLocalID,user.getUsername());
    }

    public static String insertUserInTripleStore(String userLocalID, String username) {

        return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "\n" +
                "INSERT DATA \n" +
                "{\n" +
                "ars:" + userLocalID + " rdf:type ars:User;\n" +
                "ars:hasUsername \"" + username + "\".\n" +
                "}";
    }
}
