package ar.edu.unlam.ra.semanticmiddleware.security;

public class SecurityConstants {
    public static final String SECRET = "SeManTic_RaNdOm_kEy_2078";
    public static final long EXPIRATION_TIME = 3600000; // 1 Hr
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
