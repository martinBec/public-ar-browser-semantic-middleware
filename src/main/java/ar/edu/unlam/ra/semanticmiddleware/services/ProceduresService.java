package ar.edu.unlam.ra.semanticmiddleware.services;

import ar.edu.unlam.ra.semanticmiddleware.daos.ProcedureDao;
import ar.edu.unlam.ra.semanticmiddleware.daos.StepDAO;
import ar.edu.unlam.ra.semanticmiddleware.model.Procedure;
import ar.edu.unlam.ra.semanticmiddleware.model.Step;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProceduresService {

    TripleStoreAccessService accessService;

    @Autowired
    public ProceduresService(TripleStoreAccessService accessService) {
        this.accessService = accessService;
    }

    public String searchProceduresContaining(String titleSubStr, int page) {
        String query = ProcedureDao.searchProcedures(titleSubStr, page);
        return accessService.perform(query);
    }

    public String getDetail(String procedureLocalId) {
        String query = ProcedureDao.getProcedureDetail(procedureLocalId);
        return accessService.perform(query);
    }

    public String searchProceduresByAuthorContaining(String authorLocalId, String titleSubStr, int page) {
        String query = ProcedureDao.searchProceduresByAuthor(authorLocalId,titleSubStr, page);
        return accessService.perform(query);
    }

    /////////////////////////////////////////////// Procedure Editor ////////////////////////////////////////////////

    public String insertProcedure(Procedure procedure, List<Step> steps, String authorLocalId) {

        String procedureLocalId = UUID.randomUUID().toString();
        String procedureTuples = ProcedureDao.procedureTuples(procedureLocalId,procedure.getTitle(),procedure.getDescription(),authorLocalId);
        Step firstStep = steps.get(0);
        String firstStepLocalId = UUID.randomUUID().toString();
        String firstStepTuples = StepDAO.firstStepTuplesStr(procedureLocalId,firstStepLocalId,firstStep.getTitle(),firstStep.getDescription());

        String previousStepLocalId = firstStepLocalId;
        StringBuilder nextStepTuplesBuilder = new StringBuilder();

        for (int i = 1; i < steps.size(); i++) {
            Step currentStep = steps.get(i);
            String currentStepLocalId = UUID.randomUUID().toString();
            nextStepTuplesBuilder.append(StepDAO.tuplesToCreateStepsToStr(currentStepLocalId, previousStepLocalId, currentStep.getTitle(), currentStep.getDescription()));
            previousStepLocalId = currentStepLocalId;
        }

        String insertProcedureQuery = ProcedureDao.insertProcedureQuery(procedureTuples,firstStepTuples,nextStepTuplesBuilder.toString());
        String insertProcedureResponseQuery = ProcedureDao.procedureOfAuthorEditorResponse(procedureLocalId);
        return accessService.performUpdate(insertProcedureQuery, insertProcedureResponseQuery);
    }

    public String editProcedure(Procedure procedure, List<Step> stepsToModify, List<Step> stepsToCreate, List<String> stepIdsToDelete) {

        List<String> procedureEditionQueries = new ArrayList<>();

        String editProcedureQuery =  this.mapProcedureEditionToQuery(procedure);
        List<String> nextStepsToEditQueries = this.mapStepsToModifyToQuery(stepsToModify);
        String nextStepToCreateQuery = this.mapStepsToCreateToQuery(stepsToCreate);
        List<String> stepsToDeleteQueries = this.mapStepsToDeleteQuery(stepIdsToDelete);

        if (!editProcedureQuery.isEmpty()) {
            procedureEditionQueries.add(editProcedureQuery);
        }

        if (!nextStepToCreateQuery.isEmpty()) {
            procedureEditionQueries.add(nextStepToCreateQuery);
        }

        procedureEditionQueries.addAll(nextStepsToEditQueries);
        procedureEditionQueries.addAll(stepsToDeleteQueries);

        String editProcedureResponseQuery = ProcedureDao.procedureOfAuthorEditorResponse(procedure.localId());
        return accessService.performMassUpdate(procedureEditionQueries.toArray(new String[0]), editProcedureResponseQuery);
    }

    private List<String> mapStepsToModifyToQuery(List<Step> stepsToModify) {
        return stepsToModify.stream()
                .map( stepToEdit -> StepDAO.editStepQuery(
                        stepToEdit.getId(),
                        stepToEdit.getPreviousStep(),
                        stepToEdit.getTitle(),
                        stepToEdit.getDescription())
                )
                .collect(Collectors.toList());
    }

    private String mapProcedureEditionToQuery(Procedure procedure) {
        return ProcedureDao.editProcedureDataQuery(
                procedure.getId(),
                procedure.getTitle(),
                procedure.getDescription(),
                procedure.getFirstStep()
        );
    }

    private String mapStepsToCreateToQuery(List<Step> stepsToCreate) {

        List<String> nextStepsToCreateTuples = stepsToCreate.stream()
                .map( stepToEdit -> StepDAO.tuplesToCreateSteps(
                        stepToEdit.getId(),
                        stepToEdit.getPreviousStep(),
                        stepToEdit.getTitle(),
                        stepToEdit.getDescription()
                )).reduce(new ArrayList<>(), (partialTupleList, element) -> {
                    partialTupleList.addAll(element);
                    return partialTupleList;
                });

        return StepDAO.createNextStepQuery(nextStepsToCreateTuples);
    }

    private List<String> mapStepsToDeleteQuery(List<String> stepIdsToDelete) {
        return stepIdsToDelete.stream()
                .map(StepDAO::deleteStepQuery)
                .collect(Collectors.toList());
    }
}

