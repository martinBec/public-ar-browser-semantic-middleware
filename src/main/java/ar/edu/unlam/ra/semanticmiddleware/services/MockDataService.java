package ar.edu.unlam.ra.semanticmiddleware.services;

import ar.edu.unlam.ra.semanticmiddleware.daos.ProcedureDao;
import ar.edu.unlam.ra.semanticmiddleware.daos.StepDAO;
import ar.edu.unlam.ra.semanticmiddleware.daos.TripleStoreUserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MockDataService {
    TripleStoreAccessService accessService;

    private final String description = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto" +
            "beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi" +
            "nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam" +
            "aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum" +
            "iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?";

    private final String procedureTitle = "Sed ut perspiciatis unde omnis iste natus doloremque laudantium ";

    @Autowired
    public MockDataService(TripleStoreAccessService accessService) {
        this.accessService = accessService;
    }

    public void createMockExample(String username, int numberOfProcedures, int numberOfSteps) {

        List<String> queries = new ArrayList<>();

        String userLocalId = java.util.UUID.randomUUID().toString();
        queries.add(TripleStoreUserDAO.insertUserInTripleStore(userLocalId, username));

        for (int i = 0; i < numberOfProcedures; i++) {

            String procedureLocalId = java.util.UUID.randomUUID().toString();
            String firstStepLocalId = java.util.UUID.randomUUID().toString();
            String procedureTitle = this.procedureTitle + (i + 1);
            queries.add(ProcedureDao.insertProcedures(procedureLocalId, procedureTitle, description,userLocalId));
            queries.add(StepDAO.createFirstStepProcedureQuery(procedureLocalId,firstStepLocalId,"Step 1",description));

            String previousLocalStepId = firstStepLocalId;

            for (int j = 1; j < (numberOfSteps); j++) {
                String stepLocalId = java.util.UUID.randomUUID().toString();
                queries.add(StepDAO.createNextStepProcedureQuery(stepLocalId, previousLocalStepId,"Step "+ (j + 1),description));
                previousLocalStepId = stepLocalId;
            }
        }

        String[] queriesArray = new String[queries.size()];
        queries.toArray(queriesArray);
        this.accessService.performMassUpdate(queriesArray);
    }


    // Single insertion
    public void createProcedure(int number,String authorLocalId,int stepsNumber) {

        String procedureLocalId = java.util.UUID.randomUUID().toString();
        String procedureTitle = this.procedureTitle + number;

        this.mockInsertProcedure(procedureLocalId,procedureTitle, description, authorLocalId);

        String previousLocalId = createFirstStep(procedureLocalId);

        for (int i = 1; i <= stepsNumber; i++) {
            previousLocalId = this.createNextStep(previousLocalId,i + 1);
        }
    }

    private  String createFirstStep(String procedureLocalId) {
        String stepLocalId = java.util.UUID.randomUUID().toString();
        this.insertFirstStep(procedureLocalId,stepLocalId,"Step 1",description);
        return stepLocalId;
    }

    private  String createNextStep(String previousStepLocalId, int stepNumber) {
        String stepLocalId = java.util.UUID.randomUUID().toString();
        this.insertNextStep(previousStepLocalId,stepLocalId,"Step " + stepNumber,description);
        return stepLocalId;
    }

    private void mockInsertProcedure(String procedureLocalId,String title,String description, String userLocalId) {
        String updateQuery = ProcedureDao.insertProcedures(procedureLocalId,title,description,userLocalId);
        accessService.performUpdate(updateQuery);
    }

    private void insertFirstStep(String procedureLocalId, String stepLocalId, String stepTitle, String stepDescription) {
        String query = StepDAO.createFirstStepProcedureQuery(procedureLocalId, stepLocalId, stepTitle, stepDescription);
        accessService.performUpdate(query);
    }

    private void insertNextStep(String previousStepLocalId, String stepLocalId, String stepTitle, String stepDescription) {
        String query = StepDAO.createNextStepProcedureQuery(stepLocalId, previousStepLocalId, stepTitle, stepDescription);
        accessService.performUpdate(query);
    }

}
