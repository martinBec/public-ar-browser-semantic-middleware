package ar.edu.unlam.ra.semanticmiddleware.controllers;

import ar.edu.unlam.ra.semanticmiddleware.model.Procedure;
import ar.edu.unlam.ra.semanticmiddleware.model.Step;
import ar.edu.unlam.ra.semanticmiddleware.model.User;
import ar.edu.unlam.ra.semanticmiddleware.requestBodies.Entities.ProcedureBodyDTO;
import ar.edu.unlam.ra.semanticmiddleware.requestBodies.Entities.StepDTO;
import ar.edu.unlam.ra.semanticmiddleware.requestBodies.edition.EditProcedureBodyDTO;
import ar.edu.unlam.ra.semanticmiddleware.requestBodies.edition.EditProcedureStepDTO;
import ar.edu.unlam.ra.semanticmiddleware.services.ProceduresService;
import ar.edu.unlam.ra.semanticmiddleware.services.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("api/v1/procedures")
public class ProceduresController {

    UserSession sessionService;
    ProceduresService procedureService;

    @Autowired
    public ProceduresController(ProceduresService procedureService, UserSession sessionService) {
        this.procedureService = procedureService;
        this.sessionService = sessionService;
    }

    @GetMapping(path = "/search", produces = "application/json")
    public String search(@RequestParam(value = "procedure_title", required = true) String procedureTitleSubString,
                         @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        return procedureService.searchProceduresContaining(procedureTitleSubString,page);
    }

    @GetMapping(path = "/detail", produces = "application/json")
    public String ProcedureDetail (@RequestParam(value = "procedure_id", required = true) String procedureLocalId) {
        return procedureService.getDetail(procedureLocalId);
    }

    @PostMapping(path = "/", consumes ="application/json", produces = "application/json")
    public String createProcedure(@RequestBody ProcedureBodyDTO procedureData){
        User user = this.sessionService.getUser();
        String authorLocalId = user.getSemanticLocalId();
        Procedure procedure = procedureData.getProcedure().build();

        List<Step> steps = procedureData.getSteps()
                                        .stream()
                                        .map(StepDTO::build)
                                        .collect(Collectors.toList());

        return this.procedureService.insertProcedure(procedure,steps,authorLocalId);
    }

    @PutMapping(path = "/", consumes ="application/json", produces = "application/json")
    public String editProcedure(@RequestBody EditProcedureBodyDTO procedureData){
        User user = this.sessionService.getUser();
        String authorLocalId = user.getSemanticLocalId(); // TODO: ASK Query to check author is editing his procedure.

        List<Step> stepsToCreate = new ArrayList<>();
        List<Step> stepsToModify = new ArrayList<>();
        List<String> stepIdsToDelete = new ArrayList<>();
        if(procedureData.getStepsToCreate() != null) {
            stepsToCreate = procedureData.getStepsToCreate()
                    .stream()
                    .map(EditProcedureStepDTO::build)
                    .collect(Collectors.toList());
        }

        if (procedureData.getStepsToModify() != null) {
            stepsToModify = procedureData.getStepsToModify()
                    .stream()
                    .map(EditProcedureStepDTO::build)
                    .collect(Collectors.toList());
        }

        if (procedureData.getStepsToDelete() != null) {
            stepIdsToDelete.addAll(procedureData.getStepsToDelete());
        }

        return this.procedureService.editProcedure(
                procedureData.getProcedure().build(),
                stepsToModify,
                stepsToCreate,
                stepIdsToDelete
        );
    }
}
