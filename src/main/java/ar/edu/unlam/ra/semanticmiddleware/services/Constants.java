package ar.edu.unlam.ra.semanticmiddleware.services;

public final class Constants {
    public static final int PAGE_SIZE = 50;
    public static final String ONTO_PREFIX = "ars:";
}
