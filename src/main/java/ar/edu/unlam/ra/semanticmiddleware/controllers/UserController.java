package ar.edu.unlam.ra.semanticmiddleware.controllers;

import ar.edu.unlam.ra.semanticmiddleware.model.User;
import ar.edu.unlam.ra.semanticmiddleware.requestBodies.UserRegisterBody;
import ar.edu.unlam.ra.semanticmiddleware.responses.AuthenticatedUserResponse;
import ar.edu.unlam.ra.semanticmiddleware.responses.BadRequestErrorResponse;
import ar.edu.unlam.ra.semanticmiddleware.security.JWTUtils;
import ar.edu.unlam.ra.semanticmiddleware.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/users")
public class UserController {

    private UserServiceImpl service;

    @Autowired
    public UserController(UserServiceImpl service) {
        this.service = service;
    }

    @PostMapping(path = "/register", consumes = "application/json", produces = "application/json")
    public ResponseEntity<AuthenticatedUserResponse> register(@RequestBody UserRegisterBody userData) {

        User user = userData.toUser();
        user = service.register(user);

        if (user == null) {
            return ResponseEntity.badRequest().body(null);
        }

        final String token = JWTUtils.generateTokenFor(user);
        final AuthenticatedUserResponse authenticatedUser = new AuthenticatedUserResponse(user.getUsername(),token);
        return ResponseEntity.ok(authenticatedUser);
    }
}
