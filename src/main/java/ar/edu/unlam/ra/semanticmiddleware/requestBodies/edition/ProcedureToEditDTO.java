package ar.edu.unlam.ra.semanticmiddleware.requestBodies.edition;

import ar.edu.unlam.ra.semanticmiddleware.model.Procedure;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ProcedureToEditDTO {
    String id;
    String title;
    String description;
    String firstStep;

    public Procedure build() {
        return new Procedure(id, title, description, firstStep);
    }
}
