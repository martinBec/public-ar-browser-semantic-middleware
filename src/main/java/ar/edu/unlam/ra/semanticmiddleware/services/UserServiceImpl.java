package ar.edu.unlam.ra.semanticmiddleware.services;

import ar.edu.unlam.ra.semanticmiddleware.daos.TripleStoreUserDAO;
import ar.edu.unlam.ra.semanticmiddleware.model.Authority;
import ar.edu.unlam.ra.semanticmiddleware.model.AuthorityTypes;
import ar.edu.unlam.ra.semanticmiddleware.model.User;
import ar.edu.unlam.ra.semanticmiddleware.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ar.edu.unlam.ra.semanticmiddleware.services.Constants.PAGE_SIZE;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.exact;

@Service
public class UserServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private final TripleStoreAccessService tripleStoreService;

    private static final ExampleMatcher matcher = ExampleMatcher.matching()
            .withMatcher("username", exact())
            .withMatcher("password", exact());

    @Autowired
    public UserServiceImpl(UserRepository userRepository, TripleStoreAccessService tripleStoreService) {
        this.userRepository = userRepository;
        this.tripleStoreService = tripleStoreService;
    }

    public List<User> findAll(Integer page) {
        final Pageable pageData = PageRequest.of(page, PAGE_SIZE);
        return this.userRepository.findAll(pageData);
    }
    public List<User> findUsersBy(String username,Integer page){
        final Pageable pageData = PageRequest.of(page, PAGE_SIZE);
        return userRepository.findUsersByUsername(username, pageData);
    }

    private User findBy(long id) {
        return this.userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("user " + id + " doesnt exist"));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = this.userRepository.findUserByUsername(username);
        return user.orElseThrow(() -> new UsernameNotFoundException("user " + username + " doesnt exist"));
    }

    public User register(User user) {
        List<Authority> authorityList = new ArrayList<Authority>();
        authorityList.add(new Authority(user.getUsername(), AuthorityTypes.USER));

        user = new User(user.getUsername(), user.getPassword(), authorityList);

        if (userRepository.exists(Example.of(user, matcher))) {
            return null;
        }

        String insertUserQuery = TripleStoreUserDAO.insertUser(user);
        this.tripleStoreService.performUpdate(insertUserQuery);

        return userRepository.save(user);
    }

    void save(User user) {
        this.userRepository.save(user);
    }

    void delete (User user) {
        this.userRepository.delete(user);
    }
}