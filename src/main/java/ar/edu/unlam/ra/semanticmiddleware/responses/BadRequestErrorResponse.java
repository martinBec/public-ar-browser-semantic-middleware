package ar.edu.unlam.ra.semanticmiddleware.responses;

import lombok.Getter;

@Getter
public class BadRequestErrorResponse {
    private String code;
    private String error;

    public BadRequestErrorResponse(String code, String error) {
        this.code = code;
        this.error = error;
    }

    public static BadRequestErrorResponse userRegistered() {
        return new BadRequestErrorResponse("USER_REGISTERED","not available");
    }
}
