package ar.edu.unlam.ra.semanticmiddleware.requestBodies.creation;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateFirstStepRequestBody {
    String procedure_id;
    String step_title;
    String step_description;
}
