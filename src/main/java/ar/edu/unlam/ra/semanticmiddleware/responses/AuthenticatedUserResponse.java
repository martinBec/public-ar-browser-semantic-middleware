package ar.edu.unlam.ra.semanticmiddleware.responses;

public final class AuthenticatedUserResponse {

    private final String username;
    private final String token;

    public AuthenticatedUserResponse(String username, String token) {
        this.username = username;
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return token;
    }
}
