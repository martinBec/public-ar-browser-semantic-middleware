package ar.edu.unlam.ra.semanticmiddleware.model;

import lombok.Getter;

@Getter
public final class Procedure {

    String id;
    String title;
    String description;
    String firstStep;

    public Procedure(String id, String title, String description, String firstStep) { //edition
        this.id = id;
        this.title = title;
        this.description = description;
        this.firstStep = firstStep;
    }

    public Procedure(String title, String description) {
        this(null, title, description,null);
    }

    public String localId() {
        return this.id.split(":")[1];
    }

}
