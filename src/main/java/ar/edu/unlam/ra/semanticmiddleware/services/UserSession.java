package ar.edu.unlam.ra.semanticmiddleware.services;

import ar.edu.unlam.ra.semanticmiddleware.daos.UserDAO;
import ar.edu.unlam.ra.semanticmiddleware.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class UserSession {

    private final UserDAO userRepository;

    @Autowired
    public UserSession(UserDAO userRepository) {
        this.userRepository = userRepository;
    }

    public User getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = (String) authentication.getPrincipal();
        return userRepository.findUserByUsername(username).get();
    }
}
