package ar.edu.unlam.ra.semanticmiddleware.requestBodies.Entities;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ProcedureBodyDTO {
    ProcedureDTO procedure;
    List<StepDTO> steps;
}
