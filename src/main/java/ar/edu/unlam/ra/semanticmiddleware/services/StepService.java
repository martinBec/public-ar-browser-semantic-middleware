package ar.edu.unlam.ra.semanticmiddleware.services;

import ar.edu.unlam.ra.semanticmiddleware.daos.StepDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public final class StepService {

    TripleStoreAccessService accessService;

    @Autowired
    public StepService(TripleStoreAccessService accessService) {
        this.accessService = accessService;
    }

    public String getStepsForProcedureLocalId(String localId) {
        String query = StepDAO.stepsForProcedureQuery(localId);
        return accessService.perform(query);
    }

    @Deprecated
    public String insertFirstStepToProcedure(String procedureLocalId, String stepTitle, String stepDescription) {
        String stepLocalId = UUID.randomUUID().toString();
        String query = StepDAO.createFirstStepProcedureQuery(procedureLocalId,stepLocalId,stepTitle,stepDescription);
        String queryResult = StepDAO.stepCreatedResponse(stepLocalId);
        return accessService.performUpdate(query,queryResult);
    }

    @Deprecated
    public String insertNextStep(String previousStepLocalId, String stepTitle, String stepDescription) {
        String stepLocalId = UUID.randomUUID().toString();
        String query = StepDAO.createNextStepProcedureQuery(stepLocalId, previousStepLocalId, stepTitle, stepDescription);
        String queryResult = StepDAO.stepCreatedResponse(stepLocalId);
        return accessService.performUpdate(query,queryResult);
    }
}
