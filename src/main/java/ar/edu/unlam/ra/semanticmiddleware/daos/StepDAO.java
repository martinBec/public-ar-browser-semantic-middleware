package ar.edu.unlam.ra.semanticmiddleware.daos;

import static ar.edu.unlam.ra.semanticmiddleware.services.Constants.ONTO_PREFIX;
import java.util.ArrayList;
import java.util.List;

public final class StepDAO {

    static public String stepsForProcedureQuery(String procedureId) {

        return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "\n" +
                "CONSTRUCT\n" +
                "{\n" +
                "    ?currentStep ars:hasStepTitle ?title;\n" +
                "                 ars:hasStepDescription ?description;\n" +
                "                 ars:previousStep ?previousStep;" +
                "                 ars:nextStep ?nextStep;\n" +
                "                 rdf:type ars:Step.\n" +
                "}\n" +
                "WHERE\n" +
                "{\n" +
                     procedureId + " ars:firstStep ?firstStep.\n" +
                "    ?currentStep  ars:previousStep+ ?firstStep.\n" +
                "    \n" +
                "    OPTIONAL {\n" +
                "         ?currentStep ars:hasStepTitle ?title;\n" +
                "                      ars:hasStepDescription ?description.\n" +
                "    }\n" +
                "    OPTIONAL {\n" +
                "         ?currentStep ars:previousStep ?previousStep.\n" +
                "    }      \n" +
                "    OPTIONAL {\n" +
                "         ?nextStep ars:previousStep ?currentStep.\n" +
                "    }      \n" +
                "}";
    }

    @Deprecated
    static public String createFirstStepProcedureQuery(String procedureLocalId, String stepLocalId, String stepTitle, String stepDescription) {
        return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "\n" +
                "INSERT DATA\n" +
                "{\n" +
                "  ars:" + procedureLocalId + " ars:firstStep ars:" + stepLocalId + ".\n" +
                "  ars:" + stepLocalId + " rdf:type ars:Step;\n" +
                "                    ars:hasStepTitle \"" + stepTitle + "\";\n" +
                "                    ars:hasStepDescription \"" + stepDescription + "\".\n" +
                "}";
    }

    @Deprecated
    static  public String createNextStepProcedureQuery(String stepLocalId, String previousStepLocalId, String stepTitle, String stepDescription) {

        return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "\n" +
                "INSERT DATA\n" +
                "{\n" +
                "  ars:" + stepLocalId + " rdf:type ars:Step;\n" +
                "                    ars:hasStepTitle \"" + stepTitle + "\";\n" +
                "                    ars:hasStepDescription \"" + stepDescription + "\";\n" +
                "                    ars:previousStep ars:" + previousStepLocalId + ".\n" +
                "}";
    }

    static public String stepCreatedResponse(String localStepId) {
        return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "CONSTRUCT\n" +
                "{\n" +
                "  ars:"+ localStepId + " rdf:type ars:Step;\n" +
                "                    ars:hasStepTitle ?title;\n" +
                "                    ars:hasStepDescription ?description.\n" +
                "}\n" +
                "WHERE\n" +
                "{\n" +
                "ars:" + localStepId + " ars:hasStepTitle ?title;\n" +
                "                  ars:hasStepDescription ?description.\n" +
                "}";
    }

    //////////////////////////////////////////////// Procedure Editor ////////////////////////////////////////////////

    static  public String createNextStepQuery(List<String> nextStepsTuples) {

        String nextStepsStatements = tuplesToStatementsString(nextStepsTuples);

        if (nextStepsStatements.isEmpty()) {
            return "";
        }

        return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "\n" +
                "INSERT DATA\n" +
                "{\n" +
                   nextStepsStatements +
                "\n}";
    }

    static  public String editStepQuery(String stepId, String previousStepId, String stepTitle, String stepDescription) {

        List<String> tuplesToInsert = new ArrayList<>();
        List<String> tuplesToDelete = new ArrayList<>();
        List<String> optionalTuplesToDelete = new ArrayList<>();

        if (stepTitle != null) {
            tuplesToInsert.add(stepId + " ars:hasStepTitle \"" + stepTitle + "\".\n");
            tuplesToDelete.add(stepId + " ars:hasStepTitle ?stepTitle.\n");
        }

        if (stepDescription != null) {
            tuplesToInsert.add(stepId + " ars:hasStepDescription \"" + stepDescription + "\".\n");
            tuplesToDelete.add(stepId + " ars:hasStepDescription ?stepDescription.\n");
        }

        if (previousStepId != null && !previousStepId.isEmpty()) {
            tuplesToInsert.add(stepId + " ars:previousStep " + previousStepId + ".\n");
            optionalTuplesToDelete.add(stepId + " ars:previousStep ?previousStep.\n");
        }

        if (previousStepId != null && previousStepId.isEmpty()) {
            // CU: if a Step of the chain is moved to first place, It will send a blank string to remove the property.
            optionalTuplesToDelete.add(stepId + " ars:previousStep ?previousStep.\n");
        }

        if (tuplesToDelete.isEmpty() && optionalTuplesToDelete.isEmpty()) {
            return "";
        }

        String insertStatements = tuplesToStatementsString(tuplesToInsert);
        String deleteStatements = tuplesToStatementsString(tuplesToDelete);
        String optionalStatements = tuplesToStatementsString(optionalTuplesToDelete);
        String optionalClausule = "";
        if (!optionalTuplesToDelete.isEmpty()) {

            optionalClausule = "OPTIONAL\n" +
                    " {\n" +
                    " " +optionalStatements +
                    " }\n";
        }

        return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "\n" +
                "DELETE\n" +
                "{\n" +
                   deleteStatements + optionalStatements +
                "}\n" +
                "INSERT\n" +
                "{\n" +
                   insertStatements +
                "}\n" +
                "WHERE\n" +
                "{\n" +
                   deleteStatements + optionalClausule +
                "}";
    }

    static  public List<String> tuplesToCreateSteps(String stepId, String previousStepId, String stepTitle, String stepDescription) {

        List<String> stepStatements = new ArrayList<>();

        if (stepTitle != null) {
            stepStatements.add("  " + stepId + " ars:hasStepTitle \"" + stepTitle + "\".\n");
        }

        if (stepDescription != null) {
            stepStatements.add("  " + stepId + " ars:hasStepDescription \"" + stepDescription + "\".\n");
        }

        if (previousStepId != null && !previousStepId.isEmpty()) {
            stepStatements.add("  " + stepId + " ars:previousStep " + previousStepId + ".\n");
        }

        if (!stepStatements.isEmpty()) {
            stepStatements.add("  " + stepId + " rdf:type ars:Step.\n");
        }

        return stepStatements;
    }

    private static String tuplesToStatementsString(List<String> tuples) {
        return tuples.stream().reduce("", (partialString, element) -> partialString + element);
    }

    static public String firstStepTuplesStr(String procedureLocalId, String stepLocalId, String stepTitle, String stepDescription) {
        return  "  ars:" + procedureLocalId + " ars:firstStep ars:" + stepLocalId + ".\n" +
                "  ars:" + stepLocalId + " rdf:type ars:Step;\n" +
                "  ars:hasStepTitle \"" + stepTitle + "\";\n" +
                "  ars:hasStepDescription \"" + stepDescription + "\".\n";
    }

    static public String tuplesToCreateStepsToStr(String stepLocalId, String previousStepLocalId, String stepTitle, String stepDescription) {
        return tuplesToStatementsString(tuplesToCreateSteps(ONTO_PREFIX + stepLocalId, ONTO_PREFIX + previousStepLocalId, stepTitle, stepDescription));
    }

    static public String deleteStepQuery(String stepId) {
        return "PREFIX ars: <http://www.raunlam.edu.ar/ontology/arsemanticprocedures#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "DELETE \n" +
                "{\n" +
                "  " + stepId + " rdf:type ars:Step;\n" +
                "                 ars:hasStepTitle ?title;\n" +
                "                 ars:hasStepDescription ?description;\n" +
                "                 ars:previousStep ?previousStep.\n" +
                "}\n" +
                "WHERE \n" +
                "{\n" +
                "  " + stepId + " rdf:type ars:Step;\n" +
                "                 ars:hasStepTitle ?title;\n" +
                "                 ars:hasStepDescription ?description.\n" +
                "    OPTIONAL {\n" +
                "        " + stepId + " ars:previousStep ?previousStep.\n" +
                "    }\n" +
                "}";
    }
}
