package ar.edu.unlam.ra.semanticmiddleware.requestBodies.Entities;

import ar.edu.unlam.ra.semanticmiddleware.model.Procedure;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcedureDTO {
    private String title;
    private String description;

    public ProcedureDTO(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Procedure build() {
        return new Procedure(this.title,this.description);
    }
}
