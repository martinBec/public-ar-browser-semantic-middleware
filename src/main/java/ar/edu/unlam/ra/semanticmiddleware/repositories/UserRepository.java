package ar.edu.unlam.ra.semanticmiddleware.repositories;

import ar.edu.unlam.ra.semanticmiddleware.model.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository {
    List<User> findAll(Pageable page);
    List<User> findUsersByUsername(String username,Pageable page);
    Optional<User> findUserByUsername(String username);
    Optional<User> findById(long id);
    User save (User user);
    Boolean delete (User user);
    Boolean exists(Example<User> user);
}

//extends PagingAndSortingRepository<User, Long>