package ar.edu.unlam.ra.semanticmiddleware.requestBodies.edition;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import java.util.List;

@Getter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class EditProcedureBodyDTO {

    ProcedureToEditDTO procedure;
    List<EditProcedureStepDTO> stepsToCreate;
    List<EditProcedureStepDTO> stepsToModify;
    List<String> stepsToDelete;
}
