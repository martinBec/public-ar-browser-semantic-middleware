package ar.edu.unlam.ra.semanticmiddleware.requestBodies;


import ar.edu.unlam.ra.semanticmiddleware.model.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class UserRegisterBody {
    private String username;
    private String password;

    public User toUser() {
        return new User(this.username,password);
    }
}
